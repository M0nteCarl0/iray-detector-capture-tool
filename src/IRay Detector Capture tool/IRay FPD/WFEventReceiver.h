#pragma once
#include "Detector.h"
#include "EventReceiver.h"
#include "IRayAttrDef.h"
#include "IRayCmdDef.h"
#include "IRayEventDef.h"
#include "IRayImage.h"
#include "IRayVariant.h"
#include <Windows.h>

typedef enum WM_MSG_IRay {

  WM_MSG_IRay_Evt_Image = WM_USER + Evt_Image,
  WM_MSG_IRay_Evt_TaskResult_Succeed = WM_USER + Evt_TaskResult_Succeed,
  WM_MSG_IRay_Evt_TaskResult_Failed = WM_USER + Evt_TaskResult_Failed,
  WM_MSG_IRay_Evt_TaskResult_Canceled = WM_USER + Evt_TaskResult_Canceled,
  WM_MSG_IRay_Evt_Image_Abandon = WM_USER + Evt_Image_Abandon,
  WM_MSG_IRay_Evt_CommFailure = WM_USER + Evt_CommFailure,
  WM_MSG_IRay_Evt_Exp_Prohibit = WM_USER + Evt_Exp_Prohibit,
  WM_MSG_IRay_Evt_Exp_Enable = WM_USER + Evt_Exp_Enable,
  WM_MSG_IRay_Evt_Exp_Timeout = WM_USER + Evt_Exp_Timeout,
  WM_MSG_IRay_Evt_WaitImage_Timeout = WM_USER + Evt_WaitImage_Timeout,
  WM_MSG_IRay_Evt_GeneralInfo = WM_USER + Evt_GeneralInfo,
  WM_MSG_IRay_Evt_GeneralError = WM_USER + Evt_GeneralError,
  WM_MSG_IRay_Evt_TemperatureHigh = WM_USER + Evt_TemperatureHigh,
};

class WFEventReceiver : public iEventReceiver {
public:
  HWND _WinHandle;
  CDetector *_FPD;
  WFEventReceiver(void);
  ~WFEventReceiver(void);
  virtual void SDKHandler(int nDetectorID, int nEventID, int nEventLevel,
                          const char *pszMsg, int nParam1, int nParam2,
                          int nPtrParamLen, void *pParam) override;
};
