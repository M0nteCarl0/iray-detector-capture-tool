#include "WFEventReceiver.h"

WFEventReceiver::WFEventReceiver(void) : _WinHandle(nullptr), _FPD(nullptr) {}

WFEventReceiver::~WFEventReceiver(void) {}

void WFEventReceiver::SDKHandler(int nDetectorID, int nEventID, int nEventLevel,
                                 const char *pszMsg, int nParam1, int nParam2,
                                 int nPtrParamLen, void *pParam) {

  if (_WinHandle == NULL) {
    return;
  }

  switch (nEventID) {
  case Evt_Image: {
    PostMessage(_WinHandle, WM_MSG_IRay_Evt_Image, (WPARAM)pParam, NULL);
    break;
  }

  case Evt_TemperatureHigh: {
    PostMessage(_WinHandle, WM_MSG_IRay_Evt_TemperatureHigh, (WPARAM)pszMsg,
                nParam1);
    break;
  }

  case Evt_TaskResult_Succeed: {
    PostMessage(_WinHandle, WM_MSG_IRay_Evt_TaskResult_Succeed, (WPARAM)pszMsg,
                nParam1);
    break;
  }

  case Evt_TaskResult_Failed: {
    PostMessage(_WinHandle, WM_MSG_IRay_Evt_TaskResult_Failed, (WPARAM)pszMsg,
                nParam1);
    break;
  }

  case Evt_TaskResult_Canceled: {
    PostMessage(_WinHandle, WM_MSG_IRay_Evt_TaskResult_Canceled, (WPARAM)pszMsg,
                nParam1);
    break;
  }

  case Evt_Image_Abandon: {
    PostMessage(_WinHandle, WM_MSG_IRay_Evt_Image_Abandon, (WPARAM)pszMsg,
                NULL);
    break;
  }

  case Evt_CommFailure: {
    PostMessage(_WinHandle, WM_MSG_IRay_Evt_Image_Abandon, (WPARAM)pszMsg,
                NULL);
    break;
  }

  case Evt_Exp_Prohibit: {
    PostMessage(_WinHandle, WM_MSG_IRay_Evt_Exp_Prohibit, (WPARAM)pszMsg, NULL);
    break;
  }

  case Evt_Exp_Enable: {
    PostMessage(_WinHandle, WM_MSG_IRay_Evt_Exp_Enable, (WPARAM)pszMsg, NULL);
    break;
  }

  case Evt_Exp_Timeout: {
    PostMessage(_WinHandle, WM_MSG_IRay_Evt_Exp_Timeout, (WPARAM)pszMsg, NULL);
    break;
  }

  case Evt_WaitImage_Timeout: {
    PostMessage(_WinHandle, WM_MSG_IRay_Evt_WaitImage_Timeout, (WPARAM)pszMsg,
                NULL);
    break;
  }

  case Evt_GeneralInfo: {
    PostMessage(_WinHandle, WM_MSG_IRay_Evt_GeneralInfo, (WPARAM)pszMsg, NULL);
    break;
  }
  case Evt_GeneralError: {
    PostMessage(_WinHandle, WM_MSG_IRay_Evt_GeneralError, (WPARAM)pszMsg, NULL);
    break;
  }

  default:
    break;
  }
}
