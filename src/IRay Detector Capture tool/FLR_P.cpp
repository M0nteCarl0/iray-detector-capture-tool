#include "FLR_P.h"

#define FLR_HEADER_MARKER      ((uint16_t) ('R' << 8) | 'F')

	/************************************************/
	FLR_P::FLR_P(void):_Resolution(0),_Size(0)
	{
		InitializeCriticalSection(&_CriticalSection);
	
		_ReadingBuffer = new char[4096];
		_ImageBuff = nullptr;
		
	}
	/************************************************/
	FLR_P::~FLR_P(void)
	{
		DeleteCriticalSection(&_CriticalSection);
		if(_ImageBuff)
		{
			_ImageBuff = nullptr;
			delete [] _ImageBuff;
			
		}

		if (_ReadingBuffer)
		{
			_ReadingBuffer = nullptr;
			delete[] _ReadingBuffer;
		}
	}
	/************************************************/
	bool FLR_P:: Make(const char* Filename,const char* RawSource)
	{
		uint16_t* ImageBuff = nullptr;
		string Finput = RawSource;
		string Fout = Filename;
		Finput+=".raw";
		Fout  +=".flr";
		FLE_HEADER FH;
		FH._fBpp = 16;
		FH._fBitsOffset = 0;
		FH._fType  = FLR_HEADER_MARKER;
		_FIO = fstream(Finput,std::ios::in|std::ios::binary|std::ios::ate);
	     size_t _FSS =(size_t) _FIO.tellg();
		 ImageBuff = new uint16_t[_FSS/2]; 
		 _FIO.close();
		 _FIO = fstream(Finput,std::ios::in|std::ios::binary);
		 size_t ImageSize = (size_t)sqrt((double)_FSS /sizeof(uint16_t));
		FH._fHeight = FH._fWidth = ImageSize;
		_FIO.read((char*)ImageBuff,_FSS);
		_FIO.close();
		_FIO.open(Fout,std::ios::out|std::ios::binary);
		WriteHeader(FH);
		WriteData(ImageBuff,_FSS);
		_FIO.close();
		delete[] ImageBuff;
        _ImageBuff = NULL;
		return true;

	}
	/************************************************/
	bool FLR_P::Make(const char* Filename,uint16_t* Data,size_t Datasize)
	{
		bool flag = true;
		EnterCriticalSection(&_CriticalSection);
		string Fout = Filename;
		uint16_t* ChData = new uint16_t[Datasize/2];
		Fout  +=".flr";
		FLE_HEADER FH;
		FLE_HEADER FC;
		FH._fBpp = 16;
		FH._fBitsOffset = 0;
		FH._fType  = FLR_HEADER_MARKER;
		size_t ImageSize =(size_t)  sqrt((double)Datasize /sizeof(uint16_t));
		FH._fHeight = FH._fWidth = ImageSize;
		_FIO.open(Fout,std::ios::out|std::ios::binary);
		WriteHeader(FH);
		WriteData(Data,Datasize);
		_FIO.flush();
		_FIO.close();
		_FIO.open(Fout,std::ios::in|std::ios::binary);
		ReadHeader(FC);
		ReadData(ChData,Datasize);
		_FIO.close();
		if(!Check(ChData,Data,Datasize/2))
		{

			flag = false;
		}
		delete [] ChData;
		LeaveCriticalSection(&_CriticalSection);
		return flag;
	}
	/************************************************/
	bool  FLR_P::Make2(const char* Filename,uint16_t* Data,size_t Datasize,const char* Filename1,uint16_t* Data1,size_t Datasize1)
	{
		bool flag = false;
		if (Make(Filename, Data, Datasize) && Make(Filename1, Data1, Datasize1))
		{
			flag = true;
		}
		return flag;
	}
	/************************************************/
	bool FLR_P::Make2(const char* Filename,const char* RawSource,const char* Filename1,const char* RawSource1)
	{
		bool flag = false;
		if (Make(Filename, RawSource) && Make(Filename1, RawSource1))
		{
			flag = true;

		}
		return flag;
	}
	/************************************************/
	bool FLR_P::Open(const char* Filename)
	{
		EnterCriticalSection(&_CriticalSection);
		if(_ImageBuff)
		{
			delete [] _ImageBuff;
		}
		 bool Flag = false;
		_FileName = Filename;
		char Byte;
		ios::sync_with_stdio(false);
		_FIO = fstream(Filename,std::ios::in|std::ios::binary);
		_FIO.rdbuf()->pubsetbuf(_ReadingBuffer, 4096);
        if(!_FIO.fail())
        {
		 
			_FIO.seekg(0,ios::end);
			_Size = _FIO.tellg();
			_FIO.seekg(0,ios::beg);
		ReadHeader(_HeadF);
		_Height = _HeadF._fHeight;
		_Width = _HeadF._fWidth;
		if (_HeadF._fType == FLR_HEADER_MARKER)
		{
			Flag = true;
			_ImageSize = _Size - sizeof(_HeadF);
			_ImageBuff = new uint16_t[_HeadF._fWidth * _HeadF._fHeight];
			ReadData(_ImageBuff, _Size - sizeof(_HeadF));
		}
        }
		LeaveCriticalSection(&_CriticalSection);
		return Flag;
	}

	bool FLR_P::SaveChanges(void)
	{
		bool flag = false;
		EnterCriticalSection(&_CriticalSection);
		if (_FIO.is_open())
		{
			_FIO.seekg(0, ios_base::beg);
			WriteHeader(_HeadF);
			WriteData(_ImageBuff, (_HeadF._fHeight  * _HeadF._fHeight) * 2);

			flag = true;
		}
		
		LeaveCriticalSection(&_CriticalSection);
		return flag;
	}

	void FLR_P::Close(void)
	{
		EnterCriticalSection(&_CriticalSection);
		if (_FIO.is_open())
		{
			_FIO.flush();
			_FIO.close();
		}

		if (_ImageBuff)
		{
			delete[] _ImageBuff;
			_ImageBuff = nullptr;
		}

		LeaveCriticalSection(&_CriticalSection);
	}

	/************************************************/
	void FLR_P::ReadHeader(FLE_HEADER_R Head)
	{
		_FIO.read((char*)&Head,sizeof(Head));
	}

	/************************************************/
	void FLR_P::ReadData(uint16_t* Data,size_t Datasize)
	{
		_FIO.read((char*)Data,Datasize);
	}
	/************************************************/
	size_t FLR_P::GetFileSize(void)
	{
		return(size_t)_FIO.tellg();
	}
	/************************************************/
	template<typename T>  bool FLR_P:: Check(T* In,T* Out,size_t Count)
	{
		bool Res = true;
		size_t ERC = 0;
		for(size_t i = 0;i<Count;i++)
		{
			if(In[i]!=Out[i])
			{

				ERC++;
			}
		
       }
		if(ERC!=0)
		{
			Res = false;

		}
		return Res;
	}
	/************************************************/
	void FLR_P:: WriteHeader(FLE_HEADER_R Head)
	{
		_FIO.write((char*)&Head,sizeof(Head));
	}
	/************************************************/
	void FLR_P:: WriteData(uint16_t* Data,size_t Datasize)
	{
		_FIO.write((char*)Data,Datasize);
	}
	/************************************************/
