#pragma once
#include "FLR_P.h"
#include "IRay FPD\Common.h"
#include "IRay FPD\Detector.h"
#include "IRay FPD\EventReceiver.h"
#include "IRay FPD\IRayEnumDef.h"
#include "IRay FPD\IRayImage.h"
#include "IRay FPD\WFEventReceiver.h"
#include <time.h>>
namespace IRayDetectorCapturetool {

using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;

/// <summary>
/// ������ ��� Form1
/// </summary>
public
ref class MainView : public System::Windows::Forms::Form {
public:
  CDetector *_CurFPD;
  WFEventReceiver *_EvReceiver;
  FLR_P *_SvImage;
  int _TotalFrameCounter;
  int _WrongFrameCounter;

private:
  System::Windows::Forms::ToolStripStatusLabel ^
      ExternalExp__toolStripStatusLabel;

private:
  System::Windows::Forms::Label ^ label2;

private:
  System::Windows::Forms::Label ^ FPDCurrT2_label;

private:
  System::Windows::Forms::Label ^ label4;

private:
  System::Windows::Forms::Label ^ FPDTMax_label;

private:
  System::Windows::Forms::Button ^ FPDReadT2_button;

public:
private:
  System::Windows::Forms::Timer ^ FPDState_timer;

public:
  MainView(void) {
    InitializeComponent();
    InitCore();
  }

  inline void InitCore(void) {
    AttrResult result;
    FPDRESULT Res;
    _CurFPD = nullptr;
    _SvImage = nullptr;
    _EvReceiver = nullptr;
    _TotalFrameCounter = _WrongFrameCounter = 0;
    char CurrentPath�[255] = {0};
    _SvImage = new FLR_P();
    _EvReceiver = new WFEventReceiver();
    _EvReceiver->_WinHandle = (HWND)this->Handle.ToPointer();
    GetCurrentDirectory(255, CurrentPath�);
    strcat(CurrentPath�, "\\work_dir\\Venu1717X\\");
    FPDState_timer->Start();
    _CurFPD = CDetector::CreateDetector(CurrentPath�);
    TotalFramesCount_toolStripStatusLabel->Text = _TotalFrameCounter.ToString();
    WrongFramesCount_toolStripStatusLabel->Text = _WrongFrameCounter.ToString();
    StopMeasure_button->Enabled = false;
    Snapshot_button->Enabled = false;
    FPDReadT2_button->Enabled = false;
    FPDSerialNumber_label->Text = "";
    FPDIP_label->Text = "";
    FPDWidth_label->Text = "";
    FPDHeight_label->Text = "";
    FPDModel_label->Text = "";
    FPDCurrT2_label->Text = "";
    FPDTMax_label->Text = "";
    FPDCommandState_toolStripStatusLabel->Text = "";
    ExternalExp__toolStripStatusLabel->Text = "";
    if (_CurFPD != nullptr || _CurFPD != INVALID_HANDLE_VALUE) {
      _CurFPD->RegisterHandler(_EvReceiver);
      _EvReceiver->_FPD = _CurFPD;
      Res = _CurFPD->Connect();
      PrintFPDInformation(_CurFPD);
      InitializeFPDForExtTrg(_CurFPD);
      WaitReadyDetector(_CurFPD);
      ApplyOffsetCalibration(_CurFPD);
      WaitReadyDetector(_CurFPD);
      StateFPD_toolStripStatusLabel->Text = "SW wait user actions!";
    }
    Res = _CurFPD->Disconnect();
  }

  void ApplyOffsetCalibration(CDetector *_CurFPD) {
    FPDRESULT Res;
    int PostProcessFlags = 0;
    PostProcessFlags &= ~CDetector::OFFSETMASK;
    PostProcessFlags |= Enm_CorrectOp_HW_PostOffset;
    Res = _CurFPD->SetCorrectionOption(PostProcessFlags);
    if ((Err_TaskPending != Res) && (Err_OK != Res)) {
      ExternalExp__toolStripStatusLabel->Text = "Apply correction error";
    } else {
      ExternalExp__toolStripStatusLabel->Text = "Apply correction succesfull";
    }
  }

  void WaitReadyDetector(CDetector *_CurFPD) {
    AttrResult result;
    do {
      _CurFPD->GetAttr(Attr_State, result);
    } while (Enm_State_Ready != result.nVal);
  }

  void PrintFPDInformation(CDetector *_CurFPD) {
    AttrResult result;
    FPDRESULT Res;

    _CurFPD->GetAttr(Cfg_TemperatureHighThreshold, result);
    FPDTMax_label->Text = String::Format("{0} C", result.nVal);

    _CurFPD->GetAttr(Cfg_SN, result);
    FPDSerialNumber_label->Text = gcnew String(result.strVal);
    _CurFPD->GetAttr(Cfg_RemoteIP, result);
    FPDIP_label->Text = gcnew String(result.strVal);
    _CurFPD->GetAttr(Attr_Prod_FullWidth, result);
    FPDWidth_label->Text = result.nVal.ToString();
    _CurFPD->GetAttr(Attr_Prod_FullHeight, result);
    FPDHeight_label->Text = result.nVal.ToString();
    _CurFPD->GetAttr(Cfg_ProductNo, result);
    FPDModel_label->Text = result.nVal.ToString();
    if (result.nVal != Enm_Prd_Venu1717X) {
      MessageBox::Show("Connected unsupported type FPD or FPD not connected to "
                       "Host! SW in uncoditional state! SW close! ");
      this->Close();
    }
    Snapshot_button->Enabled = true;
  }

  bool InitializeFPDForExtTrg(CDetector *_CurFPD) {
    AttrResult result;
    FPDRESULT Res;
    bool flag = true;
    WaitReadyDetector(_CurFPD);
    _CurFPD->SetAttr(Attr_UROM_TriggerMode_W, Enm_TriggerMode_Prep);
    WaitReadyDetector(_CurFPD);
    _CurFPD->GetAttr(Attr_UROM_TriggerMode, result);
    WaitReadyDetector(_CurFPD);
    if (result.nVal != Enm_TriggerMode_Prep) {
      flag = false;
    }
    _CurFPD->SetAttr(Attr_UROM_PrepCapMode_W, Enm_PrepCapMode_ClearAcq);
    WaitReadyDetector(_CurFPD);
    _CurFPD->GetAttr(Attr_UROM_PrepCapMode, result);
    WaitReadyDetector(_CurFPD);
    if (result.nVal != Enm_PrepCapMode_ClearAcq) {
      flag = false;
    }
    _CurFPD->SetAttr(Attr_UROM_HvgPrepOn_W, Enm_SignalLevel_High);
    WaitReadyDetector(_CurFPD);
    _CurFPD->GetAttr(Attr_UROM_HvgPrepOn, result);
    WaitReadyDetector(_CurFPD);
    if (result.nVal != Enm_SignalLevel_High) {
      flag = false;
    }
    _CurFPD->SetAttr(Attr_UROM_HvgXRayEnable_W, Enm_SignalLevel_High);
    WaitReadyDetector(_CurFPD);
    _CurFPD->GetAttr(Attr_UROM_HvgXRayEnable, result);
    WaitReadyDetector(_CurFPD);
    if (result.nVal != Enm_SignalLevel_High) {
      flag = false;
    }
    _CurFPD->SetAttr(Attr_UROM_HvgXRayOn_W, Enm_SignalLevel_High);
    WaitReadyDetector(_CurFPD);
    _CurFPD->GetAttr(Attr_UROM_HvgXRayOn, result);
    WaitReadyDetector(_CurFPD);
    if (result.nVal != Enm_SignalLevel_High) {
      flag = false;
    }
    _CurFPD->SetAttr(Attr_UROM_OutModeCapTrig_W, Enm_OutModeCapTrig_X_ON);
    WaitReadyDetector(_CurFPD);
    _CurFPD->GetAttr(Attr_UROM_OutModeCapTrig, result);
    WaitReadyDetector(_CurFPD);
    if (result.nVal != Enm_SignalLevel_High) {
      flag = false;
    }
    _CurFPD->Invoke(Cmd_WriteUserROM);
    WaitReadyDetector(_CurFPD);
    return flag;
  }

  void SaveFrame(IRayImage *Image) {
    char Filename[255] = {0};
    time_t Time;
    tm *TimeF;
    Time = time(NULL);
    TimeF = localtime(&Time);
    size_t TotalSize = Image->nBytesPerPixel * Image->nHeight * Image->nHeight;
    unsigned short *Buffer = new unsigned short[TotalSize];
    memcpy_s(Buffer, TotalSize, Image->pData, 1);
    sprintf(Filename, "%i_%i_%i_%i_%i_%i", TimeF->tm_hour, TimeF->tm_min,
            TimeF->tm_sec, TimeF->tm_mon, TimeF->tm_mday,
            TimeF->tm_year + 1900);
    _SvImage->Make(Filename, Image->pData, TotalSize);
    delete[] Buffer;
  }

  virtual void WndProc(Message % m) override {
    WM_MSG_IRay SW = (WM_MSG_IRay)m.Msg;
    switch (SW) {
    case WM_MSG_IRay_Evt_Image: {
      IRayImage *Image = (IRayImage *)m.WParam.ToPointer();
      SaveFrame(Image);
      _TotalFrameCounter++;
      TotalFramesCount_toolStripStatusLabel->Text =
          String::Format("Total frames CNT:{0}", _TotalFrameCounter);
      Snapshot_button->Enabled = true;
      ExternalExp__toolStripStatusLabel->Text = "Exp Completed";
      break;
    }

    case WM_MSG_IRay_Evt_TemperatureHigh: {
      FPDCurrT2_label->BackColor = System::Drawing::Color::Green;
      break;
    }

    case WM_MSG_IRay_Evt_TaskResult_Succeed: {
      FPDCommandState_toolStripStatusLabel->Text = "TaskResult Succeed";
      int Param1 = m.LParam.ToInt32();
      AttrResult result;
      switch (Param1) {
      case Cmd_ReadTemperature: {
        _CurFPD->GetAttr(Attr_RdResult_T1, result);
        FPDCurrT2_label->Text = String::Format("{0:F2} C", result.fVal);
        break;
      }
      default:
        break;
      }
      break;
    }
    case WM_MSG_IRay_Evt_TaskResult_Failed: {
      FPDCommandState_toolStripStatusLabel->Text = "TaskResult Failed";
      break;
    }
    case WM_MSG_IRay_Evt_TaskResult_Canceled: {
      FPDCommandState_toolStripStatusLabel->Text = "TaskResult Canceled:";
      break;
    }
    case WM_MSG_IRay_Evt_Image_Abandon: {
      WrongFramesCount_toolStripStatusLabel->Text =
          String::Format("Bad frames CNT:{0}", _WrongFrameCounter++);
      break;
    }
    case WM_MSG_IRay_Evt_CommFailure: {
      WrongFramesCount_toolStripStatusLabel->Text =
          String::Format("Bad frames CNT:{0}", _WrongFrameCounter++);
      break;
    }
    case WM_MSG_IRay_Evt_Exp_Prohibit: {
      ExternalExp__toolStripStatusLabel->Text = "Exp Prohibit";
      break;
    }
    case WM_MSG_IRay_Evt_Exp_Enable: {
      ExternalExp__toolStripStatusLabel->Text = "Exp Enable";
      break;
    }
    case WM_MSG_IRay_Evt_Exp_Timeout: {
      ExternalExp__toolStripStatusLabel->Text = "Exp Timeout";
      break;
    }
    case WM_MSG_IRay_Evt_WaitImage_Timeout: {
      WrongFramesCount_toolStripStatusLabel->Text =
          String::Format("Bad frames CNT:{0}", _WrongFrameCounter++);
      break;
    }
    case WM_MSG_IRay_Evt_GeneralInfo: {
      char BufferStr[255] = {0};
      strcpy_s(BufferStr, (char *)m.WParam.ToPointer());
      StateFPD_toolStripStatusLabel->Text = gcnew String(BufferStr);
      break;
    }
    case WM_MSG_IRay_Evt_GeneralError: {
      char BufferStr[255] = {0};
      strcpy_s(BufferStr, (char *)m.WParam.ToPointer());
      StateFPD_toolStripStatusLabel->Text = gcnew String(BufferStr);
      break;
    }
    default:
      break;
    }

    Form::WndProc(m);
  }

protected:
  ~MainView() {
    FPDState_timer->Stop();
    if (components) {
      delete components;
    }

    if (_SvImage) {
      delete _SvImage;
    }

    if (_CurFPD) {
      _CurFPD->Disconnect();
      _CurFPD->UnRegisterHandler(_EvReceiver);
      // delete _EvReceiver;
    }
  }

private:
  System::Windows::Forms::StatusStrip ^ statusStrip1;

private:
  System::Windows::Forms::Button ^ StartMeasure_button;

private:
  System::Windows::Forms::Button ^ StopMeasure_button;

private:
  System::Windows::Forms::Button ^ Snapshot_button;

private:
  System::Windows::Forms::ToolStripStatusLabel ^ StateFPD_toolStripStatusLabel;

private:
  System::Windows::Forms::ToolStripStatusLabel ^
      TotalFramesCount_toolStripStatusLabel;

protected:
private:
private:
private:
private:
private:
private:
  System::Windows::Forms::ToolStripStatusLabel ^
      WrongFramesCount_toolStripStatusLabel;

private:
  System::Windows::Forms::ToolStripStatusLabel ^
      FPDCommandState_toolStripStatusLabel;

private:
private:
private:
  System::Windows::Forms::GroupBox ^ groupBox1;

private:
  System::Windows::Forms::Label ^ FPDSerialNumber_label;

private:
private:
  System::Windows::Forms::Label ^ label3;

private:
  System::Windows::Forms::Label ^ FPDModel_label;

private:
private:
  System::Windows::Forms::Label ^ label1;

private:
  System::Windows::Forms::Label ^ FPDHeight_label;

private:
private:
  System::Windows::Forms::Label ^ FPDWidth_label;

private:
private:
  System::Windows::Forms::Label ^ label6;

private:
  System::Windows::Forms::Label ^ label5;

private:
  System::Windows::Forms::Label ^ FPDIP_label;

private:
private:
  System::Windows::Forms::Label ^ label10;

private:
  System::ComponentModel::IContainer ^ components;

private:
  /// <summary>
  /// ��������� ���������� ������������.
  /// </summary>

#pragma region Windows Form Designer generated code
  /// <summary>
  /// ������������ ����� ��� ��������� ������������ - �� ���������
  /// ���������� ������� ������ ��� ������ ��������� ����.
  /// </summary>
  void InitializeComponent(void) {
    this->components = (gcnew System::ComponentModel::Container());
    this->statusStrip1 = (gcnew System::Windows::Forms::StatusStrip());
    this->StateFPD_toolStripStatusLabel =
        (gcnew System::Windows::Forms::ToolStripStatusLabel());
    this->TotalFramesCount_toolStripStatusLabel =
        (gcnew System::Windows::Forms::ToolStripStatusLabel());
    this->WrongFramesCount_toolStripStatusLabel =
        (gcnew System::Windows::Forms::ToolStripStatusLabel());
    this->FPDCommandState_toolStripStatusLabel =
        (gcnew System::Windows::Forms::ToolStripStatusLabel());
    this->ExternalExp__toolStripStatusLabel =
        (gcnew System::Windows::Forms::ToolStripStatusLabel());
    this->StartMeasure_button = (gcnew System::Windows::Forms::Button());
    this->StopMeasure_button = (gcnew System::Windows::Forms::Button());
    this->Snapshot_button = (gcnew System::Windows::Forms::Button());
    this->groupBox1 = (gcnew System::Windows::Forms::GroupBox());
    this->label4 = (gcnew System::Windows::Forms::Label());
    this->FPDTMax_label = (gcnew System::Windows::Forms::Label());
    this->label2 = (gcnew System::Windows::Forms::Label());
    this->FPDCurrT2_label = (gcnew System::Windows::Forms::Label());
    this->FPDIP_label = (gcnew System::Windows::Forms::Label());
    this->label10 = (gcnew System::Windows::Forms::Label());
    this->FPDHeight_label = (gcnew System::Windows::Forms::Label());
    this->FPDWidth_label = (gcnew System::Windows::Forms::Label());
    this->label6 = (gcnew System::Windows::Forms::Label());
    this->label5 = (gcnew System::Windows::Forms::Label());
    this->FPDSerialNumber_label = (gcnew System::Windows::Forms::Label());
    this->label3 = (gcnew System::Windows::Forms::Label());
    this->FPDModel_label = (gcnew System::Windows::Forms::Label());
    this->label1 = (gcnew System::Windows::Forms::Label());
    this->FPDState_timer =
        (gcnew System::Windows::Forms::Timer(this->components));
    this->FPDReadT2_button = (gcnew System::Windows::Forms::Button());
    this->statusStrip1->SuspendLayout();
    this->groupBox1->SuspendLayout();
    this->SuspendLayout();
    //
    // statusStrip1
    //
    this->statusStrip1->Items->AddRange(
        gcnew cli::array<System::Windows::Forms::ToolStripItem ^>(5){
            this->StateFPD_toolStripStatusLabel,
            this->TotalFramesCount_toolStripStatusLabel,
            this->WrongFramesCount_toolStripStatusLabel,
            this->FPDCommandState_toolStripStatusLabel,
            this->ExternalExp__toolStripStatusLabel});
    this->statusStrip1->Location = System::Drawing::Point(0, 117);
    this->statusStrip1->Name = L"statusStrip1";
    this->statusStrip1->Size = System::Drawing::Size(698, 22);
    this->statusStrip1->TabIndex = 0;
    this->statusStrip1->Text = L"statusStrip1";
    //
    // StateFPD_toolStripStatusLabel
    //
    this->StateFPD_toolStripStatusLabel->Name =
        L"StateFPD_toolStripStatusLabel";
    this->StateFPD_toolStripStatusLabel->Size = System::Drawing::Size(123, 17);
    this->StateFPD_toolStripStatusLabel->Text = L"��������� ���������";
    //
    // TotalFramesCount_toolStripStatusLabel
    //
    this->TotalFramesCount_toolStripStatusLabel->Name =
        L"TotalFramesCount_toolStripStatusLabel";
    this->TotalFramesCount_toolStripStatusLabel->Size =
        System::Drawing::Size(118, 17);
    this->TotalFramesCount_toolStripStatusLabel->Text = L"��������� �������";
    //
    // WrongFramesCount_toolStripStatusLabel
    //
    this->WrongFramesCount_toolStripStatusLabel->Name =
        L"WrongFramesCount_toolStripStatusLabel";
    this->WrongFramesCount_toolStripStatusLabel->Size =
        System::Drawing::Size(119, 17);
    this->WrongFramesCount_toolStripStatusLabel->Text = L"��������� �������";
    //
    // FPDCommandState_toolStripStatusLabel
    //
    this->FPDCommandState_toolStripStatusLabel->Name =
        L"FPDCommandState_toolStripStatusLabel";
    this->FPDCommandState_toolStripStatusLabel->Size =
        System::Drawing::Size(143, 17);
    this->FPDCommandState_toolStripStatusLabel->Text = L"��� ��������� �������";
    //
    // ExternalExp__toolStripStatusLabel
    //
    this->ExternalExp__toolStripStatusLabel->Name =
        L"ExternalExp__toolStripStatusLabel";
    this->ExternalExp__toolStripStatusLabel->Size =
        System::Drawing::Size(176, 17);
    this->ExternalExp__toolStripStatusLabel->Text =
        L"ExternalExp_toolStripStatusLabel";
    //
    // StartMeasure_button
    //
    this->StartMeasure_button->Location = System::Drawing::Point(12, 12);
    this->StartMeasure_button->Name = L"StartMeasure_button";
    this->StartMeasure_button->Size = System::Drawing::Size(110, 23);
    this->StartMeasure_button->TabIndex = 1;
    this->StartMeasure_button->Text = L"����� ���������";
    this->StartMeasure_button->UseVisualStyleBackColor = true;
    this->StartMeasure_button->Click +=
        gcnew System::EventHandler(this, &MainView::StartMeasure_button_Click);
    //
    // StopMeasure_button
    //
    this->StopMeasure_button->Location = System::Drawing::Point(12, 41);
    this->StopMeasure_button->Name = L"StopMeasure_button";
    this->StopMeasure_button->Size = System::Drawing::Size(110, 23);
    this->StopMeasure_button->TabIndex = 2;
    this->StopMeasure_button->Text = L"���� ���������";
    this->StopMeasure_button->UseVisualStyleBackColor = true;
    this->StopMeasure_button->Click +=
        gcnew System::EventHandler(this, &MainView::StopMeasure_button_Click);
    //
    // Snapshot_button
    //
    this->Snapshot_button->Location = System::Drawing::Point(128, 12);
    this->Snapshot_button->Name = L"Snapshot_button";
    this->Snapshot_button->Size = System::Drawing::Size(104, 23);
    this->Snapshot_button->TabIndex = 3;
    this->Snapshot_button->Text = L"������";
    this->Snapshot_button->UseVisualStyleBackColor = true;
    this->Snapshot_button->Click +=
        gcnew System::EventHandler(this, &MainView::Snapshot_button_Click);
    //
    // groupBox1
    //
    this->groupBox1->Controls->Add(this->label4);
    this->groupBox1->Controls->Add(this->FPDTMax_label);
    this->groupBox1->Controls->Add(this->label2);
    this->groupBox1->Controls->Add(this->FPDCurrT2_label);
    this->groupBox1->Controls->Add(this->FPDIP_label);
    this->groupBox1->Controls->Add(this->label10);
    this->groupBox1->Controls->Add(this->FPDHeight_label);
    this->groupBox1->Controls->Add(this->FPDWidth_label);
    this->groupBox1->Controls->Add(this->label6);
    this->groupBox1->Controls->Add(this->label5);
    this->groupBox1->Controls->Add(this->FPDSerialNumber_label);
    this->groupBox1->Controls->Add(this->label3);
    this->groupBox1->Controls->Add(this->FPDModel_label);
    this->groupBox1->Controls->Add(this->label1);
    this->groupBox1->Location = System::Drawing::Point(238, 12);
    this->groupBox1->Name = L"groupBox1";
    this->groupBox1->Size = System::Drawing::Size(460, 100);
    this->groupBox1->TabIndex = 4;
    this->groupBox1->TabStop = false;
    this->groupBox1->Text = L"�������� FPD";
    //
    // label4
    //
    this->label4->AutoSize = true;
    this->label4->Location = System::Drawing::Point(193, 41);
    this->label4->Name = L"label4";
    this->label4->Size = System::Drawing::Size(61, 13);
    this->label4->TabIndex = 13;
    this->label4->Text = L"FPD Max T";
    //
    // FPDTMax_label
    //
    this->FPDTMax_label->AutoSize = true;
    this->FPDTMax_label->BorderStyle =
        System::Windows::Forms::BorderStyle::Fixed3D;
    this->FPDTMax_label->Location = System::Drawing::Point(260, 41);
    this->FPDTMax_label->Name = L"FPDTMax_label";
    this->FPDTMax_label->Size = System::Drawing::Size(37, 15);
    this->FPDTMax_label->TabIndex = 12;
    this->FPDTMax_label->Text = L"label2";
    //
    // label2
    //
    this->label2->AutoSize = true;
    this->label2->Location = System::Drawing::Point(210, 20);
    this->label2->Name = L"label2";
    this->label2->Size = System::Drawing::Size(44, 13);
    this->label2->TabIndex = 11;
    this->label2->Text = L"FPD T2";
    //
    // FPDCurrT2_label
    //
    this->FPDCurrT2_label->AutoSize = true;
    this->FPDCurrT2_label->BorderStyle =
        System::Windows::Forms::BorderStyle::Fixed3D;
    this->FPDCurrT2_label->Location = System::Drawing::Point(260, 20);
    this->FPDCurrT2_label->Name = L"FPDCurrT2_label";
    this->FPDCurrT2_label->Size = System::Drawing::Size(37, 15);
    this->FPDCurrT2_label->TabIndex = 10;
    this->FPDCurrT2_label->Text = L"label2";
    //
    // FPDIP_label
    //
    this->FPDIP_label->AutoSize = true;
    this->FPDIP_label->BorderStyle =
        System::Windows::Forms::BorderStyle::Fixed3D;
    this->FPDIP_label->FlatStyle = System::Windows::Forms::FlatStyle::System;
    this->FPDIP_label->Location = System::Drawing::Point(136, 20);
    this->FPDIP_label->Name = L"FPDIP_label";
    this->FPDIP_label->Size = System::Drawing::Size(19, 15);
    this->FPDIP_label->TabIndex = 9;
    this->FPDIP_label->Text = L"IP";
    //
    // label10
    //
    this->label10->AutoSize = true;
    this->label10->Location = System::Drawing::Point(113, 20);
    this->label10->Name = L"label10";
    this->label10->Size = System::Drawing::Size(17, 13);
    this->label10->TabIndex = 8;
    this->label10->Text = L"IP";
    //
    // FPDHeight_label
    //
    this->FPDHeight_label->AutoSize = true;
    this->FPDHeight_label->BorderStyle =
        System::Windows::Forms::BorderStyle::Fixed3D;
    this->FPDHeight_label->FlatStyle =
        System::Windows::Forms::FlatStyle::System;
    this->FPDHeight_label->Location = System::Drawing::Point(140, 84);
    this->FPDHeight_label->Name = L"FPDHeight_label";
    this->FPDHeight_label->Size = System::Drawing::Size(132, 15);
    this->FPDHeight_label->TabIndex = 7;
    this->FPDHeight_label->Text = L"������ ����������� Y ";
    //
    // FPDWidth_label
    //
    this->FPDWidth_label->AutoSize = true;
    this->FPDWidth_label->BorderStyle =
        System::Windows::Forms::BorderStyle::Fixed3D;
    this->FPDWidth_label->FlatStyle = System::Windows::Forms::FlatStyle::System;
    this->FPDWidth_label->Location = System::Drawing::Point(140, 61);
    this->FPDWidth_label->Name = L"FPDWidth_label";
    this->FPDWidth_label->Size = System::Drawing::Size(129, 15);
    this->FPDWidth_label->TabIndex = 6;
    this->FPDWidth_label->Text = L"������ ����������� X";
    //
    // label6
    //
    this->label6->AutoSize = true;
    this->label6->Location = System::Drawing::Point(7, 84);
    this->label6->Name = L"label6";
    this->label6->Size = System::Drawing::Size(127, 13);
    this->label6->TabIndex = 5;
    this->label6->Text = L"������ ����������� Y";
    //
    // label5
    //
    this->label5->AutoSize = true;
    this->label5->Location = System::Drawing::Point(7, 61);
    this->label5->Name = L"label5";
    this->label5->Size = System::Drawing::Size(127, 13);
    this->label5->TabIndex = 4;
    this->label5->Text = L"������ ����������� X";
    //
    // FPDSerialNumber_label
    //
    this->FPDSerialNumber_label->AutoSize = true;
    this->FPDSerialNumber_label->BorderStyle =
        System::Windows::Forms::BorderStyle::Fixed3D;
    this->FPDSerialNumber_label->FlatStyle =
        System::Windows::Forms::FlatStyle::System;
    this->FPDSerialNumber_label->Location = System::Drawing::Point(59, 39);
    this->FPDSerialNumber_label->Name = L"FPDSerialNumber_label";
    this->FPDSerialNumber_label->Size = System::Drawing::Size(29, 15);
    this->FPDSerialNumber_label->TabIndex = 3;
    this->FPDSerialNumber_label->Text = L"S/N";
    //
    // label3
    //
    this->label3->AutoSize = true;
    this->label3->Location = System::Drawing::Point(7, 39);
    this->label3->Name = L"label3";
    this->label3->Size = System::Drawing::Size(27, 13);
    this->label3->TabIndex = 2;
    this->label3->Text = L"S/N";
    //
    // FPDModel_label
    //
    this->FPDModel_label->AutoSize = true;
    this->FPDModel_label->BorderStyle =
        System::Windows::Forms::BorderStyle::Fixed3D;
    this->FPDModel_label->FlatStyle = System::Windows::Forms::FlatStyle::System;
    this->FPDModel_label->Location = System::Drawing::Point(59, 20);
    this->FPDModel_label->Name = L"FPDModel_label";
    this->FPDModel_label->Size = System::Drawing::Size(48, 15);
    this->FPDModel_label->TabIndex = 1;
    this->FPDModel_label->Text = L"������";
    //
    // label1
    //
    this->label1->AutoSize = true;
    this->label1->Location = System::Drawing::Point(7, 20);
    this->label1->Name = L"label1";
    this->label1->Size = System::Drawing::Size(46, 13);
    this->label1->TabIndex = 0;
    this->label1->Text = L"������";
    //
    // FPDState_timer
    //
    this->FPDState_timer->Interval = 500;
    this->FPDState_timer->Tick +=
        gcnew System::EventHandler(this, &MainView::FPDState_timer_Tick);
    //
    // FPDReadT2_button
    //
    this->FPDReadT2_button->Location = System::Drawing::Point(12, 68);
    this->FPDReadT2_button->Name = L"FPDReadT2_button";
    this->FPDReadT2_button->Size = System::Drawing::Size(214, 23);
    this->FPDReadT2_button->TabIndex = 5;
    this->FPDReadT2_button->Text = L"������ ����������� FPD";
    this->FPDReadT2_button->UseVisualStyleBackColor = true;
    this->FPDReadT2_button->Click +=
        gcnew System::EventHandler(this, &MainView::FPDReadT2_button_Click);
    //
    // MainView
    //
    this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
    this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
    this->ClientSize = System::Drawing::Size(698, 139);
    this->Controls->Add(this->FPDReadT2_button);
    this->Controls->Add(this->groupBox1);
    this->Controls->Add(this->Snapshot_button);
    this->Controls->Add(this->StopMeasure_button);
    this->Controls->Add(this->StartMeasure_button);
    this->Controls->Add(this->statusStrip1);
    this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::Fixed3D;
    this->MaximizeBox = false;
    this->Name = L"MainView";
    this->SizeGripStyle = System::Windows::Forms::SizeGripStyle::Hide;
    this->Text = L"IRay FPD Pult";
    this->statusStrip1->ResumeLayout(false);
    this->statusStrip1->PerformLayout();
    this->groupBox1->ResumeLayout(false);
    this->groupBox1->PerformLayout();
    this->ResumeLayout(false);
    this->PerformLayout();
  }
#pragma endregion
private:
  System::Void Snapshot_button_Click(System::Object ^ sender,
                                     System::EventArgs ^ e) {
    FPDRESULT Res;
    AttrResult result;
    _CurFPD->Connect();
    FPDState_timer->Enabled = true;
    int PostProcessFlags = 0;
    WaitReadyDetector(_CurFPD);
    _CurFPD->GetAttr(Attr_State, result);
    if (Enm_State_Ready == result.nVal) {
      ApplyOffsetCalibration(_CurFPD);
      WaitReadyDetector(_CurFPD);
      Res = _CurFPD->PrepAcquire();
      if (Res == Err_TaskPending) {
        Snapshot_button->Enabled = false;
        FPDCommandState_toolStripStatusLabel->Text =
            gcnew String(_CurFPD->GetErrorInfo(Res).c_str());
      } else {
        FPDCommandState_toolStripStatusLabel->Text =
            gcnew String(_CurFPD->GetErrorInfo(Res).c_str());
      }
    } else {
      MessageBox::Show("FPD not ready for capture! Please wait and try again!");
    }
  }

private:
  System::Void FPDState_timer_Tick(System::Object ^ sender,
                                   System::EventArgs ^ e) {
    AttrResult result;
    FPDRESULT Res;

    _CurFPD->GetAttr(Attr_State, result);
  INIT:
    switch (result.nVal) {
    case Enm_State_Unknown:
      StateFPD_toolStripStatusLabel->BackColor = System::Drawing::Color::Orange;
      StateFPD_toolStripStatusLabel->Text =
          " FPD State Unknown. Wait user action to begin work.";
      Snapshot_button->Enabled = false;
      break;
    case Enm_State_Ready:
      StateFPD_toolStripStatusLabel->BackColor = System::Drawing::Color::Green;
      StateFPD_toolStripStatusLabel->Text = "FPD State Ready";
      Snapshot_button->Enabled = true;

      break;
    case Enm_State_Busy:
      StateFPD_toolStripStatusLabel->BackColor = System::Drawing::Color::Red;
      StateFPD_toolStripStatusLabel->Text = "FPD State Busy";
      Snapshot_button->Enabled = false;
      break;
    case Enm_State_Sleeping:
      StateFPD_toolStripStatusLabel->BackColor = System::Drawing::Color::Blue;
      StateFPD_toolStripStatusLabel->Text = "State Sleeping";
      break;
    }
  }

private:
  System::Void StartMeasure_button_Click(System::Object ^ sender,
                                         System::EventArgs ^ e) {
    FPDRESULT Res;
    AttrResult result;
    _CurFPD->Connect();
    WaitReadyDetector(_CurFPD);
    ApplyOffsetCalibration(_CurFPD);
    WaitReadyDetector(_CurFPD);
    FPDState_timer->Enabled = true;
    StartMeasure_button->Enabled = false;
    StopMeasure_button->Enabled = true;
    FPDReadT2_button->Enabled = true;
    _CurFPD->GetAttr(Attr_State, result);
    if (Enm_State_Ready == result.nVal) {
      FPDState_timer->Enabled = true;
    } else {
      MessageBox::Show("FPD not ready for capture! Please wait or try again!");
    }
  }

private:
  System::Void StopMeasure_button_Click(System::Object ^ sender,
                                        System::EventArgs ^ e) {
    FPDRESULT Res;
    StartMeasure_button->Enabled = true;
    StopMeasure_button->Enabled = false;
    Snapshot_button->Enabled = false;
    FPDReadT2_button->Enabled = false;
    FPDState_timer->Stop();
    _CurFPD->Disconnect();
  }

private:
  System::Void FPDReadT2_button_Click(System::Object ^ sender,
                                      System::EventArgs ^ e) {
    FPDRESULT Res = _CurFPD->Invoke(Cmd_ReadTemperature);
    if (Res == Err_TaskPending) {
    }
  }
}; // namespace IRayDetectorCapturetool
} // namespace IRayDetectorCapturetool
